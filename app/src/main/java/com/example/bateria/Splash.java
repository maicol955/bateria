package com.example.bateria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.bateria.ui.login.LoginActivity;

public class Splash extends AppCompatActivity {
    //Constante de tipo entero que representa el tiempo en
    //milisegundos que se muestra nuestra actividad al usuario.
    static int TIMEOUT_MILLIS = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Asigna el archivo XML que va a ligarse como diseño de la pantalla.

        setContentView(R.layout.activity_splash);
        //Quitamos la accion bar de la pantalla Splash.

        getSupportActionBar().hide();
        //La clase handler permite enviar objetos de tipo Runnable.
        //Un Objeto de tipo runnable da el significado a un hilo de proceso
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definicion abstracta de una operacion a realizar.
                //Puede ser usado para una activity, broadcast receiver, servicios
                Intent i = new Intent(Splash.this, LoginActivity.class);
                startActivity(i);
                //Finaliza esta activity.
                finish();
            }
        }, TIMEOUT_MILLIS);

    }
}
